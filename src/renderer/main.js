import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import utils from './lib/utils'

import db from '../database'
import fs from 'fs-extra'

import ElementUI from 'element-ui'
Vue.use(ElementUI);

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.config.productionTip = false
Vue.prototype.$db = db
Vue.prototype.$fs = fs
Vue.prototype.$utils = utils

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
