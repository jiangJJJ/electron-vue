import { ipcMain, dialog } from 'electron'

ipcMain.on('open-file-dialog', function (event) {
  dialog.showOpenDialog({
    properties: ['openDirectory', 'showHiddenFiles', 'createDirectory']
  }, function (files) {
    if (files) {
      event.sender.send('dialog-selected-file', files)
    }
  })
})

ipcMain.on('set-miniwin-position', function (event, width) {
  iconWindow.setPosition(width - 150, 50)
})

ipcMain.on('drag-file-from-mini', function (event, e) {
  mainWindow.webContents.send('drag-file-from-mini', e)
})

ipcMain.on('show-main-window', function (event) {
  mainWindow.show()
})

ipcMain.on('hide-mini-window', function (event) {
  iconWindow.hide()
})
