import { app, BrowserWindow } from 'electron'
// import db from '../database'
import './events.js'
import { createTray, createMenu } from './tool.js'
import pkg from '../../package.json'

//  使打包后的应用有Windows平台的应用通知功能
if (process.platform === 'win32') {
  app.setAppUserModelId(pkg.build.appId)
}


/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

global.mainWindow = null
global.iconWindow = null
let tray = null
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`
const miniWinURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080#/mini`
  : `file://${__dirname}/index.html#/mini`

global.createWindow = function () {
  iconWindow = new BrowserWindow({
    height: 100,
    width: 100,
    x: -1000,
    y: 50,
    maximizable: false,
    frame: false,
    resizable: false,
    transparent: true,
    fullscreen: false,
    fullscreenable: false,
  })
  iconWindow.loadURL(miniWinURL)
  iconWindow.webContents.closeDevTools();
  iconWindow.on('closed', () => {
    iconWindow = null
  })
  iconWindow.on('focus', () => {
    iconWindow.webContents.send('show-title', true)
  })
  iconWindow.on('blur', () => {
    iconWindow.webContents.send('show-title', false)
  })

  let options = {
    height: 563,
    useContentSize: true,
    width: 800,
    frame: true,
    show: false,
    titleBarStyle: 'hidden',
    parent: iconWindow,
  }
  if (process.platform === 'win32') { // 针对windows平台做出不同的配置
    options.show = true   // 创建即展示
    options.frame = false // 创建一个frameless窗口
  }
  if (process.env.NODE_ENV === 'development') {
    // options.frame = true
  }

  mainWindow = new BrowserWindow(options)

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
    iconWindow.close()
  })
}

//  保证 应用只有一个实例
const isSecondInstance = app.makeSingleInstance((commandLine, workingDirectory) => {
  if (iconWindow) {
    if (iconWindow.isMinimized()) {
      iconWindow.restore()
    }
    iconWindow.focus()
  }
  if (mainWindow) {
    if (mainWindow.isMinimized()) {
      mainWindow.restore()
    }
    mainWindow.focus()
  }
})
if (isSecondInstance) {
  app.quit()
}


app.on('ready', () => {
  createWindow()
  createMenu()
  createTray()
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    // app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
