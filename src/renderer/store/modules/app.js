import Vue from 'vue'
import db from '../../../database'

const state = {
  setting: db.get('setting').value(),
  showTitle: true
}

const getters = {
  setting: state => state.setting,
  showTitle: state => state.showTitle,
}

const mutations = {
  UPDATEAPPSETTING (state, payload) {
    for (let key in payload.settings) {
      Vue.set(state, ['' + key], payload.settings['' + key])
    }
  },
}

const actions = {
  updateAppSetting({commit}, settings) {
    commit({
      type: 'UPDATEAPPSETTING',
      settings: settings
    })
  }
}

export default {
  state,
  mutations,
  actions,
  getters,
}
