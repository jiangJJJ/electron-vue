export default {
  formatFileObj: function (files) {
    let filesArr = []
    files.forEach(item => {   //  发现file对象传不过去
      let obj = {
        name: item.name,
        path: item.path,
        type: item.type,
        size: item.size,
        lastModified: item.lastModified
      }

      filesArr.push(obj)
    })

    return filesArr
  }
}
