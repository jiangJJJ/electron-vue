export default {
  mounted () {
    this.disableDragEvent()
  },
  data () {
    return {
      dropzone: null
    }
  },
  methods: {
    disableDragEvent () {
      window.addEventListener('dragenter', this.disableDrag, false)
      window.addEventListener('dragover', this.disableDrag)
      window.addEventListener('drop', this.disableDrag)
    },
    disableDrag (e) {
      if (!this.dropzone) {
        this.dropzone = Array.from(document.getElementsByClassName('candrag'))
      }

      let isChildren = false
      for (let i in this.dropzone) {
        if (this.dropzone[i].contains(e.target)) {
          isChildren = true
          break
        }
      }

      if (!isChildren) {
        e.preventDefault()
        e.dataTransfer.effectAllowed = 'none'
        e.dataTransfer.dropEffect = 'none'
      }

    },
    dropMultFiles (files, path) {
      return new Promise((resolve, reject) => {
        let promiseArr = []

        Array.from(files).forEach(item => {
          promiseArr.push(this.$fs.copy(item.path, path + '/' + item.name, {
            overwrite: false,
            errorOnExist: true
          }))
        })

        Promise.all(promiseArr.map(p => p.catch(e => e))).then((data) => {
          let errArr = []
          data.forEach(item => {
            if (item && item.message) {
              errArr.push(item.message)
            }
          })

          let message = ''
          let successCount = data.length - errArr.length
          if (successCount) {
            message += `<p style="margin-bottom: 5px">成功操作 ${successCount} 个文件。</p>`
          }

          if (errArr.length) {
            message += `<p>失败：${errArr.join('；')}</p>`
          }

          this.$message({
            showClose: true,
            dangerouslyUseHTMLString: true,
            message: message
          });

          if (successCount) {
            this.getAllList && this.getAllList()
            resolve()
          }

          reject()

        }, err => {})
      })

    }
  },
  beforeDestroy () {
    window.removeEventListener('dragenter', this.disableDrag, false)
    window.removeEventListener('dragover', this.disableDrag)
    window.removeEventListener('drop', this.disableDrag)
  }
}
