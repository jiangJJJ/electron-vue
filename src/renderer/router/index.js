import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/',
      name: 'index',
      component: require('@/pages/Home').default
    },
    {
      path: '/setting',
      name: 'setting',
      meta: {
        needCheck: false,
      },
      component: require('@/pages/settingForm').default
    },
    {
      path: '/mini',
      name: 'mini',
      meta: {
        showTitle: false,
        needCheck: false,
      },
      component: require('@/pages/miniWindow').default
    },
  ]
})

router.beforeEach((to, from ,next) => {
  let matched = [].slice.call(to.matched)
  let target  = matched.pop()

  // let setting = store.getters.setting
  // let needCheck = target.meta.needCheck == undefined ? true : target.meta.needCheck
  // if (!setting.rootPath && needCheck) {
  //   next({name: 'setting', replace: true})
  //   return
  // }

  let showTitle = target.meta.showTitle == undefined ? true : target.meta.showTitle
  store.dispatch('updateAppSetting', {
    showTitle: showTitle
  })

  next()
})


export default router
