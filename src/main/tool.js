const {Tray, Menu, app} = require('electron')

export const createTray = function () {
  const menubarPic = `${__static}/logo.png`
  let tray = new Tray(menubarPic)
  const contextMenu = Menu.buildFromTemplate([
    { label: '显示',
      click () {
        if (mainWindow) {
          mainWindow.show()
          iconWindow.show()
        } else {
          createWindow()
        }
      }
    },
    { role: 'quit', label: '退出'}
  ])

  tray.setToolTip('宇宙超级无敌大美女写的爱普🎃')
  tray.setContextMenu(contextMenu)

  tray.on('click', () => {
    if (mainWindow) {
      mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show()
      iconWindow.show()
    } else {
      createWindow()
    }
  })

  return tray
}

export const createMenu = () => {
  if (process.env.NODE_ENV !== 'development') {
    const template = [{
      label: 'Edit',
      submenu: [
        { label: 'Undo', accelerator: 'CmdOrCtrl+Z', selector: 'undo:' },
        { label: 'Redo', accelerator: 'Shift+CmdOrCtrl+Z', selector: 'redo:' },
        { type: 'separator' },
        { label: 'Cut', accelerator: 'CmdOrCtrl+X', selector: 'cut:' },
        { label: 'Copy', accelerator: 'CmdOrCtrl+C', selector: 'copy:' },
        { label: 'Paste', accelerator: 'CmdOrCtrl+V', selector: 'paste:' },
        { label: 'Select All', accelerator: 'CmdOrCtrl+A', selector: 'selectAll:' },
        {
          label: 'Quit',
          accelerator: 'CmdOrCtrl+Q',
          click () {
            app.quit()
          }
        }
      ]
    }]
    let menu = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(menu)
  }
}
