import Datastore from 'lowdb'
import FileSync from 'lowdb/adapters/FileSync'
import path from 'path'
import fs from 'fs-extra'
import { app, remote } from 'electron' // 引入remote模块
import LodashId from 'lodash-id'

const APP = process.type === 'renderer' ? remote.app : app // 根据process.type来分辨在哪种模式使用哪种模块

const STORE_PATH = APP.getPath('userData') // 获取electron应用的用户目录
if (process.type !== 'renderer') {
  if (!fs.pathExistsSync(STORE_PATH)) { // 如果不存在路径
    fs.mkdirpSync(STORE_PATH) // 就创建
  }
}

const adapter = new FileSync(path.join(STORE_PATH, '/data.json')) // 初始化lowdb读写的json文件名以及存储路径

const db = Datastore(adapter) // lowdb接管该文件
db._.mixin(LodashId)

//  初始化 配置表
if (!db.has('setting').value()) {
  db.set('setting', {
    rootPath: '',
    pageSize: 10,
    moveWay: 1,
    showTempArea: true,
    autoRun: false,
  }).write()
}
//  初始化暂存区表
if (!db.has('fileTempList').value()) {
  db.set('fileTempList', []).write()
}


export default db // 暴露出去
